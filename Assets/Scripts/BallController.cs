﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour {
	public int force;
	Rigidbody2D	rigid;
	int scoreP1;
	int scoreP2;
	Text scoreUIP1;
	Text scoreUIP2;
	GameObject panelSelesai;
	Text txPemenang;

	// Use this for initialization
	void Start () {
		rigid = GetComponent<Rigidbody2D>();
		Vector2 arah = new Vector2(2, 0).normalized;
		rigid.AddForce(arah * force);

		scoreP1 = 0;
		scoreP2 = 0;

		scoreUIP1 = GameObject.Find("Score1").GetComponent<Text>();
		scoreUIP2 = GameObject.Find("Score2").GetComponent<Text>();

		panelSelesai = GameObject.Find("PanelSelesai");
		panelSelesai.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ResetBall () {
		transform.localPosition = new Vector2(0, 0);
		rigid.velocity = new Vector2 (0, 0);
	}

	void TampilkanScore () {
		scoreUIP1.text = scoreP1 + "";
		scoreUIP2.text = scoreP2 + "";
	}

	void CekScore() {
		if (scoreP1 == 5 || scoreP2 == 5) {
			panelSelesai.SetActive(true);
			txPemenang = GameObject.Find("Pemenang").GetComponent<Text>();
			if (scoreP1 == 5) txPemenang.text = "Player Biru Menang!";
			else txPemenang.text = "Player Merah Menang!";
			Destroy(gameObject);
		}
	}

	private void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.name == "TepiKanan") {
			scoreP1 += 1;
			TampilkanScore();
			CekScore();
			ResetBall();
			Vector2 arah = new Vector2(2, 0).normalized;
			rigid.AddForce(arah * force);
		}
		else if (coll.gameObject.name == "TepiKiri") {
			scoreP2 += 1;
			TampilkanScore();
			CekScore();
			ResetBall();
			Vector2 arah = new Vector2(-2, 0).normalized;
			rigid.AddForce(arah * force);
		}

		if (coll.gameObject.name == "Pemukul1" || coll.gameObject.name == "Pemukul2") {
			float sudut = (transform.position.y - coll.transform.position.y) * 5f;
			Vector2 arah = new Vector2(rigid.velocity.x, sudut).normalized;
			rigid.velocity = new Vector2(0, 0);
			rigid.AddForce(arah * force * 2);
		}
	}
}
